
#include "stm32f1xx_it.h"

#include "main.h"

// NonMaskableInt_IRQn
void NMI_Handler(void) {
    while (1) {
    }
}

// HardFault_IRQn
void HardFault_Handler(void) {
    while (1) {
    }
}

// MemoryManagement_IRQn
void MemManage_Handler(void) {
    while (1) {
    }
}

// USER CODE BEGIN BusFault_IRQn
void BusFault_Handler(void) {
    while (1) {
    }
}

// UsageFault_IRQn
void UsageFault_Handler(void) {
    while (1) {
    }
}

// SVCall_IRQn
// void SVC_Handler(void) {}

// DebugMonitor_IRQn
void DebugMon_Handler(void) {
}

// PendSV_IRQn
// void PendSV_Handler(void) {}

// SysTick_IRQn
// void SysTick_Handler(void) {}
