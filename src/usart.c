/*
 *  usart.c
 *
 *  Created on: 2021-09-24
 *  Author: rafau
 */

#include "usart.h"

#include "rcc.h"

void usart_init_8d_1s_no_parity(USART_TypeDef *usart, uint32_t baudrate, uint16_t interrupts) {
    struct rcc_clock_freq clocks;
    uint32_t apb_clock;

    rcc_get_clock_freq(&clocks);

    // 8 data bits, 1 stop bit, no parity and no flow control
    usart->CR1 = USART_CR1_RE | USART_CR1_TE | interrupts;
    usart->CR2 = 0x00;
    usart->CR3 = 0x00;
    usart->GTPR = 0x00;

    if ((uint32_t)usart == USART1_BASE) {
        apb_clock = clocks.apb2;
    } else {
        apb_clock = clocks.apb1;
    }

    usart->BRR = (uint16_t)(apb_clock / baudrate);
}

void usart_blocking_write(USART_TypeDef *usart, const uint8_t *buf, uint32_t len) {
    for (uint32_t i = 0; i < len; ++i) {
        while (!(usart->SR & USART_SR_TXE))
            ;

        usart->DR = buf[i];
    }

    while (!(usart->SR & USART_SR_TC))
        ;
}
