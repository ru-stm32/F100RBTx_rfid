#include "main.h"

#include "gpio.h"
#include "rcc.h"
#include "usart.h"

// FreeRTOS
#include "FreeRTOS.h"
#include "task.h"

void vLedTask(void *pvParameters);
void vUsartTask(void *pvParameters);

void usart1_init();

int main(void) {
    system_clock_config();

    NVIC_SetPriorityGrouping(__NVIC_PRIO_BITS);

    xTaskCreate(vLedTask, "ledTask", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    xTaskCreate(vUsartTask, "usartTask", configMINIMAL_STACK_SIZE, NULL, 1, NULL);

    vTaskStartScheduler();
}

void vLedTask(void *pvParameters) {
    rcc_apb2_enable(RCC_APB2ENR_IOPCEN);

    GPIOC->ODR |= P8 | P9;

    gpio_pin_config(GPIOC, P8, gpio_mode_output_PP_10MHz);
    gpio_pin_config(GPIOC, P9, gpio_mode_output_PP_10MHz);

    for (;;) {
        GPIOC->ODR ^= P8;
        GPIOC->ODR ^= P9;

        vTaskDelay(pdMS_TO_TICKS(500));
    }

    vTaskDelete(NULL);
}

void vUsartTask(void *pvParameters) {
    const char const hello[] = "usartTask created\n\r";
    const char const beb[] = "beb\n\r";

    usart1_init();

    usart_blocking_write(USART1, (const uint8_t *)hello, sizeof(hello) - 1);

    for (;;) {
        usart_blocking_write(USART1, (const uint8_t *)beb, sizeof(beb) - 1);

        vTaskDelay(pdMS_TO_TICKS(1000));
    }

    vTaskDelete(NULL);
}

void usart1_init() {
    rcc_apb2_enable(RCC_APB2ENR_IOPAEN | RCC_APB2ENR_USART1EN);

    gpio_pin_config(GPIOA, P9, gpio_mode_alternate_PP_2MHz);  // PA9 -> USART2_TX
    gpio_pin_config(GPIOA, P10, gpio_mode_input_floating);    // PA10 -> USART2_RX

    usart_init_8d_1s_no_parity(USART1, 115200, 0);
    usart_enable(USART1);
}

void system_clock_config(void) {
    // Turn on HSE
    RCC->CR |= RCC_CR_HSEON;

    // PREDIV1 input clock not divided
    RCC->CFGR2 &= ~RCC_CFGR2_PREDIV1_Msk;

    // PLLMUL: PLL input clock x 3
    RCC->CFGR &= ~RCC_CFGR_PLLMULL_Msk;
    RCC->CFGR |= RCC_CFGR_PLLMULL3;

    // Clock from PREDIV1 selected as the PLL input clock
    RCC->CFGR |= RCC_CFGR_PLLSRC;

    // Wait till HSE is ready
    while ((RCC->CR & RCC_CR_HSERDY) == 0)
        ;

    // Turn on PLL
    RCC->CR |= RCC_CR_PLLON;

    // Wait till PLL is ready
    while ((RCC->CR & RCC_CR_PLLRDY) == 0)
        ;

    // PLL selected as system clock
    RCC->CFGR |= RCC_CFGR_SW_PLL;

    // Wait till PLL is used as system clock
    while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL)
        ;

    // Turn off HSI
    RCC->CR &= ~RCC_CR_HSION;

    SystemCoreClockUpdate();
}
