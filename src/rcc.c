/*
 *  rcc.c
 *
 *  Created on: 2021-09-24
 *  Author: rafau
 */

#include "rcc.h"

#include "system_stm32f1xx.h"

void rcc_get_clock_freq(struct rcc_clock_freq *freq) {
    static __I uint8_t AHBPrescTable[8] = {1, 2, 3, 4, 6, 7, 8, 9};  // shift
    static __I uint8_t APBPrescTable[8] = {0, 0, 0, 0, 1, 2, 3, 4};  // shift
    static __I uint8_t ADCPrescTable[4] = {2, 4, 6, 8};              // divide

    if (!freq) return;

    uint32_t aux;
    uint8_t prescaler;

    SystemCoreClockUpdate();
    freq->sysclk = SystemCoreClock;

    // AHB prescaler
    aux = (RCC->CFGR & RCC_CFGR_HPRE_Msk) >> RCC_CFGR_HPRE_Pos;
    prescaler = (0x00 == aux) ? 0 : AHBPrescTable[aux & 0x07];
    freq->hclk = freq->sysclk >> prescaler;

    // APB1 prescaler
    aux = (RCC->CFGR & RCC_CFGR_PPRE1_Msk) >> RCC_CFGR_PPRE1_Pos;
    prescaler = APBPrescTable[aux & 0x07];
    freq->apb1 = freq->sysclk >> prescaler;

    // APB2 prescaler
    aux = (RCC->CFGR & RCC_CFGR_PPRE2_Msk) >> RCC_CFGR_PPRE2_Pos;
    prescaler = APBPrescTable[aux & 0x07];
    freq->apb2 = freq->sysclk >> prescaler;

    // ADC prescaler
    aux = (RCC->CFGR & RCC_CFGR_ADCPRE_Msk) >> RCC_CFGR_ADCPRE_Pos;
    prescaler = ADCPrescTable[aux & 0x03];
    freq->adc = freq->sysclk / prescaler;
}