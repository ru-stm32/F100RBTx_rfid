cmake_minimum_required (VERSION 3.6)

set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/cmake/arm-none-eabi-gcc.cmake)
include(CMakePrintHelpers)

set(PROJECT_NAME "F100RBTx_rfid")
project(${PROJECT_NAME})
enable_language(ASM)

set(CMAKE_BUILD_TYPE Debug)
# set(CMAKE_BUILD_TYPE RelWithDebInfo)

if(GCC)
    set(LINKER_OPTION "-T")
    set(LINKER_SCRIPT_SUFFIX ".ld")
else()
    message(FATAL_ERROR "This project don't support provided toolchain!")
endif()

set(SOURCE_DIR ${CMAKE_SOURCE_DIR}/src)
set(CMSIS_DIR ${CMAKE_SOURCE_DIR}/drivers/CMSIS)
set(DEVICE_DIR ${CMAKE_SOURCE_DIR}/drivers/CMSIS/Device/ST/STM32F1xx)
set(STARTUP_DIR ${CMAKE_SOURCE_DIR}/startup)
set(LINKER_DIR ${CMAKE_SOURCE_DIR}/linker)

include_directories(${CMAKE_SOURCE_DIR}/inc)
include_directories(${CMSIS_DIR}/Include)
include_directories(${DEVICE_DIR}/Include)

# FreeRTOS
include(cmake/freertos.cmake)

add_executable(${PROJECT_NAME}
    ${SOURCE_DIR}/main.c
    ${SOURCE_DIR}/stm32f1xx_it.c
    ${SOURCE_DIR}/system_stm32f1xx.c
    ${STARTUP_DIR}/startup_stm32f100xb.s
    ${SOURCE_DIR}/gpio.c
    ${SOURCE_DIR}/rcc.c
    ${SOURCE_DIR}/usart.c
    ${FreeRTOS_SOURCES}
)

set_target_properties(${PROJECT_NAME} PROPERTIES SUFFIX ".elf")

target_compile_definitions(${PROJECT_NAME} PUBLIC
    "__weak=__attribute__((weak))"
    "__packed=__attribute__((__packed__))"
    STM32F100xB
    HSI_VALUE=8000000U
    LSI_VALUE=40000U
    HSE_VALUE=8000000U
    LSE_VALUE=0
)

target_compile_options(${PROJECT_NAME} PUBLIC
    -mcpu=cortex-m3
    -mthumb
    -mno-thumb-interwork
    -mfpu=vfp
    -msoft-float
    -Wall
    -Wextra
    -fdata-sections 
    -ffunction-sections
    "$<$<CONFIG:DEBUG>:-O0;-g3;-ggdb>"
)

set(LINKER_SCRIPT ${LINKER_DIR}/STM32F100RBTx_FLASH${LINKER_SCRIPT_SUFFIX})

target_link_options(${PROJECT_NAME} PUBLIC
    ${LINKER_OPTION} ${LINKER_SCRIPT}
    -specs=nano.specs
    -Wl,--gc-section
    -g
    -ggdb3
    -mcpu=cortex-m3
    -mthumb
)

if(CMAKE_OBJCOPY)

    add_custom_command(
        TARGET ${PROJECT_NAME}
        COMMAND ${CMAKE_OBJCOPY}
        ARGS "-S" "${PROJECT_NAME}.elf" ">>" "${PROJECT_NAME}.lst"
        DEPENDS ${PROJECT_NAME}.elf
    )

    add_custom_command(
        TARGET ${PROJECT_NAME}
        COMMAND ${CMAKE_OBJCOPY} -O ihex ${PROJECT_NAME}.elf ${PROJECT_NAME}.hex
        DEPENDS ${PROJECT_NAME}.elf
    )

    add_custom_command(
        TARGET ${PROJECT_NAME}
        COMMAND ${CMAKE_OBJCOPY} -O binary ${PROJECT_NAME}.elf ${PROJECT_NAME}.bin
        DEPENDS ${PROJECT_NAME}.elf
    )

endif()

include(cmake/openocd.cmake)

if(CMAKE_OPENOCD) 
    add_custom_target(flash
        ${CMAKE_OPENOCD} -f ../openocd.cfg
        DEPENDS ${PROJECT_NAME})
endif()
