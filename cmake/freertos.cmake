
# TODO: Add free fros with FetchContent
# include(FetchContent)

# set(FreeRTOS ON)

# FetchContent_Declare(
#     FreeRTOS
#     GIT_REPOSITORY https://github.com/FreeRTOS/FreeRTOS-Kernel.git
#     GIT_TAG        V10.4.5
# )

# set(FreeRTOS_SOURCES 
# ${FreeRTOS_SOURCE_DIR}/croutine.c 
# ${FreeRTOS_SOURCE_DIR}/event_groups.c 
# ${FreeRTOS_SOURCE_DIR}/list.c 
# ${FreeRTOS_SOURCE_DIR}/queue.c 
# ${FreeRTOS_SOURCE_DIR}/stream_buffer.c 
# ${FreeRTOS_SOURCE_DIR}/tasks.c 
# ${FreeRTOS_SOURCE_DIR}/timers.c
# )

# FetchContent_MakeAvailable(FreeRTOS)

# set(CMAKE_FREERTOS_SOURCE_DIR ${FreeRTOS_SOURCE_DIR})

# include_directories(${FreeRTOS_SOURCE_DIR}/include)

set(FreeRTOS_DIR ${CMAKE_SOURCE_DIR}/FreeRTOS)
include_directories(${FreeRTOS_DIR}/include)

if (GCC)
    include_directories(${FreeRTOS_DIR}/portable/GCC/ARM_CM3)
    set(FreeRTOS_PORTABLE_PORT_SRC ${FreeRTOS_DIR}/portable/GCC/ARM_CM3/port.c)
endif()

set(FreeRTOS_SOURCES 
    ${FreeRTOS_DIR}/croutine.c 
    ${FreeRTOS_DIR}/event_groups.c 
    ${FreeRTOS_DIR}/list.c 
    ${FreeRTOS_DIR}/queue.c 
    ${FreeRTOS_DIR}/stream_buffer.c 
    ${FreeRTOS_DIR}/tasks.c 
    ${FreeRTOS_DIR}/timers.c
    ${FreeRTOS_DIR}/portable/MemMang/heap_4.c
    ${FreeRTOS_PORTABLE_PORT_SRC}
)