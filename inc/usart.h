/*
 *  usart.h
 *
 *  Created on: 2021-09-24
 *  Author: rafau
 */

#ifndef __USART_H__
#define __USART_H__

#include "stm32f1xx.h"

static inline void usart_enable(USART_TypeDef *usart) {
    usart->CR1 |= USART_CR1_UE;
}

static inline void usart_disable(USART_TypeDef *usart) {
    usart->CR1 &= ~USART_CR1_UE;
}

void usart_init_8d_1s_no_parity(USART_TypeDef *usart, uint32_t baudrate, uint16_t interrupts);
void usart_blocking_write(USART_TypeDef *usart, const uint8_t *buf, uint32_t len);

#endif /* !__USART_H__ */
