/*
 *  rcc.h
 *
 *  Created on: 2021-09-24
 *  Author: rafau
 */

#ifndef __RCC_H__
#define __RCC_H__

#include "stm32f1xx.h"

struct rcc_clock_freq {
    uint32_t sysclk;
    uint32_t hclk;
    uint32_t apb1;
    uint32_t apb2;
    uint32_t adc;
};

void rcc_get_clock_freq(struct rcc_clock_freq *freq);

static inline void rcc_apb2_enable(uint32_t mask) {
    RCC->APB2ENR |= mask;
}

static inline void rcc_apb2_disable(uint32_t mask) {
    RCC->APB2ENR &= ~mask;
}

static inline void rcc_apb1_enable(uint32_t mask) {
    RCC->APB1ENR |= mask;
}

static inline void rcc_apb1_disable(uint32_t mask) {
    RCC->APB1ENR &= ~mask;
}

#endif /* !__RCC_H__ */
